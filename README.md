    1. Einführung
    
    Dieses Programm soll die Interaktion mit der Datenbank "pythonprojekt" vereinfachen.
    "pythonprojekt" besitzt eine Tabelle mit dem Namen "Kunden", die mit Datensätzen aus dem vorangegangenen
    Datenbankprojekt "Kraut und Rüben" gefüllt ist. Dieses Programm bietet Möglichkeiten zur Erweiterung, Aktualisierung
    und Kontrolle der in "Kunden" enthaltenen Datensätze.
    
    Zum Starten der Applikation führen Sie einfach die Datei 'App.py' aus, welche sich im src Ordner befindet.
    Hierzu muss die Datenbank "pythonprojekt" bereits gestartet sein. Sollte dies nicht der Fall sein, können Sie 
    hierzu mehr Informationen unter Punkt 4 finden.
    
    
    2. Funktionen des Programms
    
    Nach Starten der Applikation werden dem Nutzer über die Kommandozeile mehrere Auswahlmöglichkeiten gegeben:
    
    *Alle Kunden anzeigen:
        Diese Funktion ruft sämtliche Einträge aus der Tabelle "Kunden" aus und gibt diese aus.
        
    *Alle Kunden exportieren:
        Diese Funktion ruft sämtliche Einträge aus der Tabelle "Kunden" aus und exportiert diese in das File 
        "Kunden.txt", welches im root-Ordner des Projektes zu finden ist. Ist diese Datei noch nicht vorhanden,
        wird sie erstellt.
    
    *Neuen Kunden einfügen:
        Nach Auswahl dieser Option bietet das Programm dem Nutzer die Option die Parameter eines neuen Kunden
        einzugeben. Dieser wird in die Datenbank eingefügt
        
    *Bestehenden Kunden löschen: 
        Diese Option entfernt einen Nutzer aus der Datenbank. Hierzu muss nur dessen Kundennummer angegeben werden.
        
    *Bestimmte Kundennummer abfragen:
        Hier kann eine Kundennummer eingegeben werden. Die Daten des zugehörigen Kunden werden ausgegeben.
        
    *Bestimmten Namen abfragen:
        Hier kann der Name eines Kunden eingegeben werden. Die Datenbank zeigt alle Datensätze unter diesem Namen.
        
    *Programm beenden
        Nach Beendigung der Datenbankinteraktion kann der Nutzer hier das Programm beenden.
        
    Jede der obenstehenden Optionen hat eine zugeordnete Nummer. Zum Auswählen einer Option, schreiben Sie einfach die
    gewünschte Nummer in die Kommandozeile und bestätigen mit Enter.
    
    Sollte die Datenbank bestimmte Eingaben nicht verarbeiten können gibt das Programm eine entsprechende Rückmeldung.
    Starten Sie danach einfach neu und korrigieren Sie die Eintragungen.
    
    
    3. Unit - Tests
    
    Zum Überprüfen der korrekten Funktionalität wurden einige Unit-Tests geschrieben. Diese sind in der Datei "test.py"
    zu finden und können nach Änderungen am Programm oder vor Start der Arbeit ausgeführt werden.
    
    Hierzu können Sie einfach die Datei "test.py" ausführen.
    
    
    4. Datenbank "pythonprojekt"
    
    Zum Starten der Datenbank, führen Sie die Datei "sql_dumb_database_create.sql" (zu finden unter 
    automatisierungsauftrag/pythonprojekt/src/) in ihrer Entwicklungsumgebung für Datenbanken aus.
    Als nächstes können Sie die Datei "pythonprojectload.sql" ausführen, um die Datenbank mit den Startdaten
    zu füllen. Danach können "createRoles.sql" und "createUsers.sql" ausgeführt werden, um die Berechtigungen
    der Datenbank festzulegen.
