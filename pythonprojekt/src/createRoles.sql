USE pythonprojekt;

CREATE ROLE root;

GRANT ALL PRIVILEGES ON pythonprojekt.* TO root;

FLUSH PRIVILEGES;
