DROP DATABASE IF EXISTS pythonprojekt; 
CREATE DATABASE IF NOT EXISTS pythonprojekt;
USE pythonprojekt;

CREATE TABLE KUNDE (
    KUNDENNR        INTEGER NOT NULL,
    NACHNAME        VARCHAR(50),
    VORNAME         VARCHAR(50),
    GEBURTSDATUM	  DATE,
	 STRASSE         VARCHAR(50),
	 HAUSNR			  VARCHAR(6),			
    PLZ             VARCHAR(5),
    TELEFON         VARCHAR(25),
    EMAIL           VARCHAR(50)
    );
    
/******************************************************************************/
/***                              Primary Keys                              ***/
/******************************************************************************/
    
    ALTER TABLE KUNDE ADD PRIMARY KEY (KUNDENNR);