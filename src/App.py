from src.main.abfragen import *

if __name__ == "__main__":
    introAusgeben()
    befehlAbfragen()
    """
    Dieses Programm soll die Interaktion mit der Datenbank "pythonprojekt" vereinfachen.
    "pythonprojekt" besitzt eine Tabelle mit dem Namen "Kunden", die mit Datensaetzen aus dem vorangegangenen
    Datenbankprojekt "Kraut und Rueben" gefuellt ist. Dieses Programm bietet Moeglichkeiten zur Erweiterung, Aktualisierung
    und Kontrolle der in "Kunden" enthaltenen Datensaetze.
    
    
    Nach Starten der Applikation werden dem Nutzer mehrere Auswahlmoeglichkeiten gegeben:
    
    *Alle Kunden anzeigen:
        Diese Funktion ruft saemtliche Eintraege aus der Tabelle "Kunden" aus und gibt diese aus.
        
    *Alle Kunden exportieren:
        Diese Funktion ruft saemtliche Eintraege aus der Tabelle "Kunden" aus und exportiert diese in das File 
        "Kunden.txt", welches im root-Ordner des Projektes zu finden ist. Ist diese Datei noch nicht vorhanden,
        wird sie erstellt.
    
    *Neuen Kunden einfuegen:
        Nach Auswahl dieser Option bietet das Programm dem Nutzer die Option die Parameter eines neuen Kunden
        einzugeben. Dieser wird in die Datenbank eingefuegt
        
    *Bestehenden Kunden loeschen: 
        Diese Option entfernt einen Nutzer aus der Datenbank. Hierzu muss nur dessen Kundennummer angegeben werden.
        
    *Bestimmte Kundennummer abfragen:
        Hier kann eine Kundennummer eingegeben werden. Die Daten des zugehoerigen Kunden werden ausgegeben.
        
    *Bestimmten Namen abfragen:
        Hier kann der Name eines Kunden eingegeben werden. Die Datenbank zeigt alle Datensaetze unter diesem Namen.
        
    *Programm beenden
        Nach Beendigung der Datenbankinteraktion kann der Nutzer hier das Programm beenden.
        
    Jede der obenstehenden Optionen hat eine zugeordnete Nummer. Zum Auswaehlen einer Option, schreiben Sie einfach die
    gewuenschte Nummer in die Kommandozeile und bestaetigen mit Enter.
    
    Sollte die Datenbank bestimmte Eingaben nicht verarbeiten koennen gibt das Programm eine entsprechende Rueckmeldung.
    Starten Sie danach einfach neu und korrigieren Sie die Eintragungen.
    
    Zum Überpruefen der korrekten Funktionalitaet wurden einige Unit-Tests geschrieben. Diese sind in der Datei "test.py"
    zu finden und koennen nach Änderungen am Programm oder vor Start der Arbeit ausgefuehrt werden.
    """
