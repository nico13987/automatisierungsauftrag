import unittest

from src.App import hinzufuegenKunde, loescheKunde, gibAlleKundenAlsStringZurueck, zeigeBestimmtenKunden


class TestSum(unittest.TestCase):

    def testKundeEinfuegen(self):
        # Einfuegen des Testdatensatzes
        hinzufuegenKunde("'9999'", "'Tester'", "'Toaster'", "'1974-04-03'", "'Gelsenkirchener Strasse'", "'78'",
                         "'09854'",
                         "'040/123456'",
                         "'testertoaster@purgatory.hell'")

        # Ergebnisszustand der Datenbank als String speichern
        AlleKundenString = gibAlleKundenAlsStringZurueck()
        print("String aller Kunden:")
        print(AlleKundenString)
        print("")

        # Bekannten Datensatz als String speichern
        BekannterSubString: str = "(9999, 'Tester', 'Toaster', datetime.date(1974, 4, 3), 'Gelsenkirchener Strasse', '78', '09854', '040/123456', 'testertoaster@purgatory.hell')"

        # ueberpruefen ob der Datensatz im aktuellen Zustand der Datenbank vorhanden ist
        if BekannterSubString in AlleKundenString:
            result = True
            print("der Substring ist im Hauptstring enthalten")
        else:
            result = False
            print("der Substring ist nicht im Hauptstring enthalten")
        print(result)

        # ueberpruefung des Variablenwertes
        self.assertTrue(result, "Der Substring ist nicht im Hauptstring enthalten!")

        # bereinigen der Testdaten
        loescheKunde("'9999'")

    def testKundeLoeschen(self):
        # Vorbereiten des Datensatzes
        hinzufuegenKunde("'9999'", "'Tester'", "'Toaster'", "'1974-04-03'", "'Gelsenkirchener Strasse'", "'78'",
                         "'09854'",
                         "'040/123456'",
                         "'testertoaster@purgatory.hell'")

        # Testdatensatz loeschen
        loescheKunde("'9999'")

        # Ergebnisszustand der Datenbank als String speichern
        AlleKundenString = gibAlleKundenAlsStringZurueck()
        print("String aller Kunden:")
        print(AlleKundenString)
        print("")

        # Bekannten Datensatz als String speichern
        BekannterSubString: str = "(9999, 'Tester', 'Toaster', datetime.date(1974, 4, 3), 'Gelsenkirchener Strasse', '78', '09854', '040/123456', 'testertoaster@purgatory.hell')"

        # ueberpruefen ob der Datensatz im aktuellen Zustand der Datenbank vorhanden ist
        if BekannterSubString in AlleKundenString:
            result = True
            print("der Substring ist im Hauptstring enthalten")
        else:
            result = False
            print("der Substring ist nicht im Hauptstring enthalten")
        print(result)

        # ueberpruefung des Variablenwertes
        self.assertFalse(result, "Der Substring ist noch im Hauptstring enthalten!")

    def testKundeabfragen(self):

        """
        Dieser Test fragt einen bekannten Datensatz aus "Kunden" ab und vergleicht das Ergebnis mit dem Soll-Datensatz.
        Sollte der bekannte Datzensatz aus der Datenbank geloescht werden, wird dieser Test fehlschlagen. In diesem Fall
        kann der entfernte Datensatz schlicht durch einen noch vorhandenen, bekannten Datensatz ersetzt werden.

        """

        # Datensatz aus Datenbank als String speichern
        kundeString = zeigeBestimmtenKunden("2001")
        print("Kundendaten:")
        print(kundeString)
        print("")

        # Bekannten Datensatz unter dieser Kundennummer als String speichern
        bekannterString: str = "(2001, 'Wellensteyn', 'Kira', datetime.date(1990, 5, 5), 'Eppendorfer Landstrasse', '104', '20249', '040/443322', 'k.wellensteyn@yahoo.de')"

        # ueberpruefung des Variablenwertes
        self.assertEqual(kundeString, bekannterString, "Der bekannte String passt nicht zu dem Kundenstring!")
