import sys

import mysql.connector

from src.main.zugangsdaten_datenbank.datenbankzugang import *


def zeigeAlleKunden():
    # verbindung herstellen
    try:
        mydb = mysql.connector.connect(
            host=const_host,
            user=const_user,
            password=const_password,
            database=const_database,
            auth_plugin=const_auth_plugin
        )
    except mysql.connector.errors.ProgrammingError:
        print("Die Datenbank verhindert den Zugriff. Bitte ueberpruefen Sie die Zugangsdaten.")
    except mysql.connector.errors.InterfaceError:
        print("Kann keine Verbindung zur Datenbank herstellen. Bitte ueberpruefen Sie ob der Server gestartet ist.")

    try:
        print(mydb)
    except UnboundLocalError:
        print("Verbindung fehlgeschlagen.")
        sys.exit(1)

    print(const_database)
    print(" ")

    # sql befehl
    cursor = mydb.cursor()
    SQLBefehl = 'SELECT * FROM KUNDE'
    cursor.execute(SQLBefehl)

    # ergebniss printen mit while loop
    row = cursor.fetchone()
    while row is not None:
        print(row)
        row = cursor.fetchone()
    print(" ")

    # verbindung trennen
    cursor.close()
    mydb.close()


def zeigeBestimmtenKunden(knr):
    # verbindung herstellen
    try:
        mydb = mysql.connector.connect(
            host=const_host,
            user=const_user,
            password=const_password,
            database=const_database,
            auth_plugin=const_auth_plugin
        )
    except mysql.connector.errors.ProgrammingError:
        print("Die Datenbank verhindert den Zugriff. Bitte ueberpruefen Sie die Zugangsdaten.")
    except mysql.connector.errors.InterfaceError:
        print("Kann keine Verbindung zur Datenbank herstellen. Bitte ueberpruefen Sie ob der Server gestartet ist.")

    try:
        print(mydb)
    except UnboundLocalError:
        print("Verbindung fehlgeschlagen.")
        sys.exit(1)

    print(const_database)
    print(" ")

    # sql befehl
    cursor = mydb.cursor()
    SQLBefehl = 'SELECT * FROM KUNDE WHERE KUNDENNR = ' + knr + ''
    try:
        cursor.execute(SQLBefehl)
    except mysql.connector.errors.DatabaseError:
        print("Die eingegebenen Daten koennen nicht von der Datenbank verarbeitet werden. "
              "Bitte ueberpruefen sie die Eingabe.")
        sys.exit(1)

    # ergebniss printen mit while loop
    stringBestimmterKunde = ""

    row = cursor.fetchone()
    while row is not None:
        print(row)
        stringBestimmterKunde += str(row)
        row = cursor.fetchone()
    print(" ")

    # verbindung trennen
    cursor.close()
    mydb.close()

    return stringBestimmterKunde

def zeigeBestimmtenNamen(nn, vn):
    # verbindung herstellen
    try:
        mydb = mysql.connector.connect(
            host=const_host,
            user=const_user,
            password=const_password,
            database=const_database,
            auth_plugin=const_auth_plugin
        )
    except mysql.connector.errors.ProgrammingError:
        print("Die Datenbank verhindert den Zugriff. Bitte ueberpruefen Sie die Zugangsdaten.")
    except mysql.connector.errors.InterfaceError:
        print("Kann keine Verbindung zur Datenbank herstellen. Bitte ueberpruefen Sie ob der Server gestartet ist.")

    try:
        print(mydb)
    except UnboundLocalError:
        print("Verbindung fehlgeschlagen.")
        sys.exit(1)

    print(const_database)
    print(" ")

    # sql befehl
    cursor = mydb.cursor()
    SQLBefehl = 'SELECT * FROM KUNDE WHERE NACHNAME = ' + nn + ' AND VORNAME = ' + vn + ''
    cursor.execute(SQLBefehl)

    # ergebniss printen mit while loop
    stringBestimmterKunde = ""

    row = cursor.fetchone()
    while row is not None:
        print(row)
        stringBestimmterKunde += str(row)
        row = cursor.fetchone()
    print(" ")

    # verbindung trennen
    cursor.close()
    mydb.close()

    return stringBestimmterKunde


def gibAlleKundenAlsStringZurueck():
    # verbindung herstellen
    try:
        mydb = mysql.connector.connect(
            host=const_host,
            user=const_user,
            password=const_password,
            database=const_database,
            auth_plugin=const_auth_plugin
        )
    except mysql.connector.errors.ProgrammingError:
        print("Die Datenbank verhindert den Zugriff. Bitte ueberpruefen Sie die Zugangsdaten.")
    except mysql.connector.errors.InterfaceError:
        print("Kann keine Verbindung zur Datenbank herstellen. Bitte ueberpruefen Sie ob der Server gestartet ist.")

    try:
        print(mydb)
    except UnboundLocalError:
        print("Verbindung fehlgeschlagen.")
        sys.exit(1)

    print(const_database)
    print(" ")

    # sql befehl
    cursor = mydb.cursor()
    SQLBefehl = 'SELECT * FROM KUNDE'
    cursor.execute(SQLBefehl)

    # ergebnis in string printen mit while loop
    alleKunden = ""

    row = cursor.fetchone()
    while row is not None:
        alleKunden += str(row)
        row = cursor.fetchone()

    # verbindung trennen
    cursor.close()
    mydb.close()

    # string zurueckgeben
    return alleKunden

def exportiereAlleKunden():
    # verbindung herstellen
    try:
        mydb = mysql.connector.connect(
            host=const_host,
            user=const_user,
            password=const_password,
            database=const_database,
            auth_plugin=const_auth_plugin
        )
    except mysql.connector.errors.ProgrammingError:
        print("Die Datenbank verhindert den Zugriff. Bitte ueberpruefen Sie die Zugangsdaten.")
    except mysql.connector.errors.InterfaceError:
        print("Kann keine Verbindung zur Datenbank herstellen. Bitte ueberpruefen Sie ob der Server gestartet ist.")

    try:
        print(mydb)
    except UnboundLocalError:
        print("Verbindung fehlgeschlagen.")
        sys.exit(1)

    print(const_database)
    print(" ")

    # sql befehl
    cursor = mydb.cursor()
    SQLBefehl = 'SELECT * FROM KUNDE'
    cursor.execute(SQLBefehl)

    # ergebniss exportieren
    file = open("../../Kunden.txt", "w")

    row = cursor.fetchone()
    while row is not None:
        file.write(str(row) + "\n")
        row = cursor.fetchone()
    print("Kundenliste exportiert")

    # verbindung trennen
    cursor.close()
    mydb.close()
