import mysql.connector

import sys

from src.main.zugangsdaten_datenbank.datenbankzugang import *


def hinzufuegenKunde(knr, nn, vn, gd, str, hnr, plz, tel, mail):
    # verbindung herstellen
    try:
        mydb = mysql.connector.connect(
            host=const_host,
            user=const_user,
            password=const_password,
            database=const_database,
            auth_plugin=const_auth_plugin
        )
    except mysql.connector.errors.ProgrammingError:
        print("Die Datenbank verhindert den Zugriff. Bitte ueberpruefen Sie die Zugangsdaten.")
    except mysql.connector.errors.InterfaceError:
        print("Kann keine Verbindung zur Datenbank herstellen. Bitte ueberpruefen Sie ob der Server gestartet ist.")

    try:
        print(mydb)
    except UnboundLocalError:
        print("Verbindung fehlgeschlagen.")
        sys.exit(1)

    print(const_database)
    print(" ")
    print("einzufuegender Kunde: " + knr, nn, vn, gd, str, hnr, plz, tel, mail)
    print(" ")

    # sql befehl
    cursor = mydb.cursor()
    SQLBefehl = 'INSERT INTO KUNDE (KUNDENNR, NACHNAME, VORNAME, GEBURTSDATUM, STRASSE, HAUSNR, PLZ, TELEFON, EMAIL) VALUES (' + knr + ', ' + nn + ', ' + vn + ', ' + gd + ', ' + str + ', ' + hnr + ', ' + plz + ', ' + tel + ', ' + mail + ')'
    try:
        cursor.execute(SQLBefehl)
    except mysql.connector.errors.DataError:
        print("Die eingegebenen Daten koennen nicht von der Datenbank verarbeitet werden. "
              "Bitte ueberpruefen sie das Format des Geburtsdatums.")
        sys.exit(1)
    except mysql.connector.errors.DatabaseError:
        print("Die eingegebenen Daten koennen nicht von der Datenbank verarbeitet werden. "
              "Bitte stellen Sie sicher, dass die Kundennummer noch nicht vergeben wurde. "
              "Nur Kundennummern vom Dateityp 'Integer' sind gestattet.")
        sys.exit(1)
    mydb.commit()

    # verbindung trennen
    cursor.close()
    mydb.close()


def loescheKunde(knr):
    # verbindung herstellen
    try:
        mydb = mysql.connector.connect(
            host=const_host,
            user=const_user,
            password=const_password,
            database=const_database,
            auth_plugin=const_auth_plugin
        )
    except mysql.connector.errors.ProgrammingError:
        print("Die Datenbank verhindert den Zugriff. Bitte ueberpruefen Sie die Zugangsdaten.")
    except mysql.connector.errors.InterfaceError:
        print("Kann keine Verbindung zur Datenbank herstellen. Bitte ueberpruefen Sie ob der Server gestartet ist.")

    try:
        print(mydb)
    except UnboundLocalError:
        print("Verbindung fehlgeschlagen.")
        sys.exit(1)

    print(const_database)
    print(" ")
    print("zu loeschender Kunde: Nr. " + knr)
    print(" ")

    # sql befehl
    cursor = mydb.cursor()
    SQLBefehl = 'DELETE FROM KUNDE WHERE KUNDENNR = ' + knr + ';'
    cursor.execute(SQLBefehl)
    mydb.commit()

    # verbindung trennen
    cursor.close()
    mydb.close()
