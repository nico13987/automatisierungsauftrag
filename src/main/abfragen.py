import sys

from src.main.datenbankzugriffe import *

from src.main.datenbankaenderungen import *


def hilfeAusgeben():
    print("")
    print("")
    print("Dieses Programm soll die Interaktion mit der Datenbank 'pythonprojekt' vereinfachen. ")
    print("Nach Starten der Applikation werden dem Nutzer mehrere Auswahlmoeglichkeiten gegeben: ")
    print("")
    print("*Alle Kunden anzeigen:")
    print("   Diese Funktion ruft saemtliche Eintraege aus der Tabelle 'Kunden' aus und gibt diese aus.")
    print("")
    print("*Alle Kunden exportieren:")
    print("   Diese Funktion ruft saemtliche Eintraege aus der Tabelle 'Kunden' aus und exportiert diese in das File")
    print("   'Kunden.txt', welches im root-Ordner des Projektes zu finden ist. Ist diese Datei noch nicht vorhanden,")
    print("   wird sie erstellt.")
    print("")
    print("*Neuen Kunden einfuegen:")
    print("   Nach Auswahl dieser Option bietet das Programm dem Nutzer die Option die Parameter eines neuen Kunden")
    print("   einzugeben. Dieser wird in die Datenbank eingefuegt")
    print("")
    print("*Bestehenden Kunden loeschen:")
    print("   Diese Option entfernt einen Nutzer aus der Datenbank. Hierzu muss nur dessen Kundennummer "
          "angegeben werden.")
    print("")
    print("*Bestimmte Kundennummer abfragen:")
    print("   Hier kann eine Kundennummer eingegeben werden. Die Daten des zugehoerigen Kunden werden ausgegeben.")
    print("")
    print("*Bestimmten Namen abfragen:")
    print("   Hier kann der Name eines Kunden eingegeben werden. Die Datenbank zeigt alle Datensaetze unter "
          "diesem Namen.")
    print("")
    print("*Programm beenden")
    print("   Nach Beendigung der Datenbankinteraktion kann der Nutzer hier das Programm beenden.")
    print("")
    print("Jede der obenstehenden Optionen hat eine zugeordnete Nummer. Zum Auswaehlen einer Option, schreiben sie ")
    print("einfach die gewuenschte Nummer in die Kommandozeile und bestaetigen mit Enter.")
    print("")
    print("")

def introAusgeben():
    print("")
    print("")
    print("Willkommen! Dieses Programm soll die Interaktion mit der Datenbank 'pythonprojekt' vereinfachen. ")
    print("Nach Starten der Applikation werden dem Nutzer mehrere Auswahlmoeglichkeiten gegeben. ")
    print("Jede der Optionen hat eine zugeordnete Nummer. Zum Auswaehlen einer Option, schreiben Sie ")
    print("einfach die gewuenschte Nummer in die Kommandozeile und bestaetigen mit Enter.")
    print("")
    print("")

def abfrageNeuerKunde():
    print("Hinzufuegen eines neuen Kunden:")

    knr = "'" + input("Wie soll die Kundennummer lauten? \n") + "'"
    nn = "'" + input("Wie lautet der Nachname des Kunden? \n") + "'"
    vn = "'" + input("Wie lautet der Vorname des Kunden? \n") + "'"
    gd = "'" + input("Wann ist der Kunde geboren? Angabe bitte in folgendem Format: 2021-02-10. \n") + "'"
    str = "'" + input("In welcher Straße wohnt der Kunde? \n") + "'"
    hnr = "'" + input("Wie lautet die Hausnummer des Kunden? \n") + "'"
    plz = "'" + input("Wie lautet die Postleitzahl des Kunden? \n") + "'"
    tel = "'" + input("Wie lautet die Telefonnummer des Kunden? \n") + "'"
    mail = "'" + input("Wie lautet die E-Mail des Kunden? \n") + "'"

    hinzufuegenKunde(knr, nn, vn, gd, str, hnr, plz, tel, mail)

    print("Der Kunde wurde hinzugefuegt!")


def abfrageKundeLoeschen():
    print("Loeschen eines bestehenden Kunden:")

    knr = "'" + input("Wie lautet die Kundennummer? \n") + "'"

    loescheKunde(knr)

    print("Der Kunde wurde geloescht!")


def abfrageBestimmterKunde():
    print("Abfrage eines bestimmten Kunden nach Kundennummer:")

    knr = "'" + input("Wie lautet die Kundennummer? \n") + "'"

    zeigeBestimmtenKunden(knr)

def abfrageBestimmterName():
    print("Abfrage eines bestimmten Kunden nach Namen:")

    nn = "'" + input("Wie lautet der Nachname des Kunden? \n") + "'"
    vn = "'" + input("Wie lautet der Vorname des Kunden? \n") + "'"

    zeigeBestimmtenNamen(nn, vn)


def befehlAbfragen():
    while True:
        befehl = input("Was soll erledigt werden? "
                       "0: Alle Kunden anzeigen, "
                       "1: Alle Kunden exportieren, "
                       "2: Neuen Kunden einfuegen, "
                       "3: Bestehenden Kunden loeschen, "
                       "4: Bestimmte Kundennummer abfragen, "
                       "5: Bestimmten Namen abfragen, "
                       "h: die Hilfe aufrufen, "
                       "99: Programm beenden. \n"
                       )
        if befehl == "0":
            zeigeAlleKunden()
        elif befehl == "1":
            exportiereAlleKunden()
        elif befehl == "2":
            abfrageNeuerKunde()
        elif befehl == "3":
            abfrageKundeLoeschen()
        elif befehl == "4":
            abfrageBestimmterKunde()
        elif befehl == "5":
            abfrageBestimmterName()
        elif befehl == "h":
            hilfeAusgeben()
        elif befehl == "99":
            print("Vielen Dank fuer Ihren Besuch! \n")
            sys.exit(0)
        else:
            print("Dieser Nummer ist keine Zahl zugeordnet! \n")
